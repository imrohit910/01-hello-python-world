# Hello Python World

## Name

Hello Python World

## Description 

**Hello World** program written in python 

## Explanation 

Python is an interpreted language so it does not need to be compiled to run but it needs an interpreter to understand and run the code. That's why on the machine that has the script or program  need to be installed the python enviroment.

Python code can be written directly in a file outside a **main** function. It will execute line like a script.
Usually in a python file it is seen the next code

```python
def func1():
    print("func1")

def func2():
    print("func2")

if __name__ == "__main__":
    func1()
    funct2()
```
That means if the file with this code is executed  the functions after `if __name__ == "__main__":` are exectuted. If the file is imported as a module in another file the functions are not executed, they are just made avaible in the new file. The file which is executed has the  `if __name__ == "__main__":`. If imported has th name of the module so the chack fails and never execute the block.


**main.py** example

```python
print("Hello Python World ouside a function or a main function")

def main():
    print("Hello Python World")

if __name__ == "__main__":
    main()
```

## Run the script 

```bash
git https://gitlab.com/python-beginner-tutorial/01-hello-python-world.git
cd 01-hello-python-world
python main.py
```
