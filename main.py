
# In python anyting can run outside of a function.
# Is not necessarily to have  a main function or any other function.

print("Hello Python World ouside a function or a main function")
# Function to call hello
def display(Name):
    print("Hello ",Name)
def main():
    print("Hello Python World")
    name=input("Enter Your name, so Python can say Hello to You \n")
    display(name);

# this means that the file is runing itself not inported as a module (import
# other file via import statement

if __name__ == "__main__":
    main()
